package org.bibsonomy.dnbimport;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.bibsonomy.common.Pair;
import org.bibsonomy.common.enums.PostUpdateOperation;
import org.bibsonomy.dnbimport.database.DnbDatabaseManager;
import org.bibsonomy.dnbimport.model.ClassificationScheme;
import org.bibsonomy.dnbimport.model.DnbPerson;
import org.bibsonomy.dnbimport.model.DnbPublication;
import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.Person;
import org.bibsonomy.model.PersonName;
import org.bibsonomy.model.Post;
import org.bibsonomy.model.Resource;
import org.bibsonomy.model.ResourcePersonRelation;
import org.bibsonomy.model.User;
import org.bibsonomy.model.enums.Gender;
import org.bibsonomy.model.enums.PersonIdType;
import org.bibsonomy.model.enums.PersonResourceRelationType;
import org.bibsonomy.model.logic.LogicInterface;
import org.bibsonomy.model.util.BibTexUtils;
import org.bibsonomy.util.ValidationUtils;


/**
 * Given two DNB-IDs a,b, such that b advised a in some phdthesis d. 
 * Insert this relation into BibSonomy using the correct relator code.
 *
 * @author tomhanika
 *
 */
public class AdvisorRelation implements Runnable {
	private LogicInterface adminLogic;
	private DnbDatabaseManager dnbDatabaseManager;
	private String userName;  // User which will insert the relation
		
	@Override
	public void run() {
		final Writer logWriter = openErrorLogFile();
		System.out.println("HELLO MARIO, do you want to play a game?");
		try {
			logWriter.append("LOGGED FOR MARIO");
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		final User user = new User();
		user.setName(userName);
	
		String A = ""; // Stumme
		String B = "212124544"; //Wille
		try {
			Person advisee = this.inferPerson(A);
			Person advisor = this.inferPerson(B);
			Post<BibTex> thesis = null;
			try {
				thesis = getAdvisedThesis(advisee); 
			} catch (Exception e) {
				logWriter.append("Getting " +  advisee.getPersonId() + "\t" + " did result in exception");
			}
			if (thesis == null){
				logWriter.append("Couldn't find unique thesis for " + advisee.getPersonId());
			}
			ResourcePersonRelation theNewRelation = new ResourcePersonRelation();
			theNewRelation.setPerson(advisor);
			theNewRelation.setPost(thesis);
			theNewRelation.setRelationType(PersonResourceRelationType.DOCTOR_VATER);
			theNewRelation.setChangedBy(user.getName());
			theNewRelation.setChangedAt(new Date());
			theNewRelation.setPersonIndex(-1);
			try {
				adminLogic.addResourceRelation(theNewRelation);
			} catch (Exception e){
				logWriter.append("Problem inserting " + A + " with " + B);
			}
		} catch (Exception e){
			System.out.println("BLOEDSINN");
		}
	}
	
	
	/**
	 * Infer person from a given dnb_id
	 * @param dnbId
	 * @return
	 */
	private Person inferPerson(String dnbId){
		Person person = adminLogic.getPersonById(PersonIdType.DNB_ID, dnbId);
		return person;
	}
	
	private Post<BibTex> getAdvisedThesis(Person p){
		List<ResourcePersonRelation> resourceRelations = 
				adminLogic.getResourceRelations().byPersonId(p.getPersonId())
				.withPosts(true)
				.withPersonsOfPosts(true) // What exactly is this thing doing anyway?
				.getIt();
		List<Post<BibTex>> authorPosts = new ArrayList<>();
		
		for (final ResourcePersonRelation resourcePersonRelation : resourceRelations) {
			// A post has always an entryptype, we may check if this type is phdthesis
			final boolean isPhdEntrytype = resourcePersonRelation.getPost()
					.getResource()
					.getEntrytype()
					.toLowerCase().endsWith("thesis");
			if (!isPhdEntrytype){
				continue; // If this post is not a phdthesis, this relator has nothing to do
			}
			String typeOfThesis = resourcePersonRelation.getPost().getResource().getMiscField("type");
			if (typeOfThesis != null && typeOfThesis == "habilitation"){
				continue; // In case we have a type entry with 'habilitation' our method is not applicable
			}
			
			if (resourcePersonRelation.getRelationType().equals(PersonResourceRelationType.AUTHOR)) {
				authorPosts.add((Post<BibTex>) resourcePersonRelation.getPost()); //Deliberately employed uncheck cast.
			}	
			
			if (authorPosts.size() != 1) 
			{
				return null; // If in the end there are more or less than 1 posts (i.e., phd-thesis) we can't relate anything
			}
		}
		return authorPosts.get(0); // Return this single post as the advised phd thesis
	}	
	
	
	
	private OutputStreamWriter openErrorLogFile() {
		try {
			return new OutputStreamWriter(new BufferedOutputStream(new FileOutputStream(new File("importErrors_" + System.currentTimeMillis() + ".log"))), "UTF-8");
		} catch (UnsupportedEncodingException | FileNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	public LogicInterface getAdminLogic() {
		return this.adminLogic;
	}

	public void setAdminLogic(LogicInterface adminLogic) {
		this.adminLogic = adminLogic;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
}