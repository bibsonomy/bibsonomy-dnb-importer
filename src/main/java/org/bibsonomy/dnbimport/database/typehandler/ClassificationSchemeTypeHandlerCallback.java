package org.bibsonomy.dnbimport.database.typehandler;

import java.sql.SQLException;

import org.bibsonomy.common.enums.GroupRole;
import org.bibsonomy.database.common.typehandler.AbstractTypeHandlerCallback;
import org.bibsonomy.dnbimport.model.ClassificationScheme;

import com.ibatis.sqlmap.client.extensions.ParameterSetter;

/**
 * An iBATIS type handler callback for {@link GroupRole}es that are mapped to
 * Strings in the database. If a GroupRole cannot be constructed based on the
 * String, then the GroupRole will be set to <code>USER</code>.<br/>
 * 
 * Almost copied from <a href=
 * "http://opensource.atlassian.com/confluence/oss/display/IBATIS/Type+Handler+Callbacks"
 * >Atlassian - Type Handler Callbacks</a>
 * 
 * @author Clemens Baier
 */
public class ClassificationSchemeTypeHandlerCallback extends AbstractTypeHandlerCallback {
	@Override
	public void setParameter(final ParameterSetter setter,
			final Object parameter) throws SQLException {
		if (parameter instanceof ClassificationScheme) {
			setter.setString(null);
		} else {
			final ClassificationScheme kind = (ClassificationScheme) parameter;
			setter.setString(kind.name());
		}
	}

	@Override
	public Object valueOf(String str) {
		str = str.toLowerCase();
		if (str.equals("22sdnb") || str.equals("sswd")) {
			return ClassificationScheme.sdnb;
		}
		try {
			return ClassificationScheme.valueOf(str);
		} catch (IllegalArgumentException ex) {
			return null;
		}
	}

}
