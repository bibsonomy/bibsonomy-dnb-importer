package org.bibsonomy.dnbimport;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * TODO: add documentation to this class
 *
 * @author tom
 * @author mho
 */
public class AdvisorImporterMain {
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("HELLO MARIO, do you want to play a game?");
		final BeanFactory beanFactory = new ClassPathXmlApplicationContext("org/bibsonomy/dnbimport/advisorImporterContext.xml");
		System.out.println("HELLO MARIO, do you want to play a game?");
		beanFactory.getBean("advisorImporter", Runnable.class).run();
	}

}
