package org.bibsonomy.dnbimport.model;

/**
 * TODO: add documentation to this class
 *
 * @author jensi
 */
public enum ClassificationScheme {
	ddr,
	fm81,
	fm03,
	sdnb
}
